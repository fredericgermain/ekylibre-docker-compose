#!/bin/sh

. ./env.sh

[ -f env_cache.sh ] && . ./env_cache.sh
if [ -z $EKYLIBRE_DATABASE_PASSWORD ]; then EKYLIBRE_DATABASE_PASSWORD=$(openssl rand -hex 16); fi
if [ -z $DEVISE_SECRET_KEY ]; then DEVISE_SECRET_KEY=$(openssl rand -hex 64); fi
if [ -z $SECRET_KEY_BASE ]; then SECRET_KEY_BASE=$(openssl rand -hex 64); fi
RAILS_ENV=production
TENANT=$EKYLIBRE_TENANT
VERBOSE=1

setupappenv() {
  [ -f env_cache.sh ] && rm env_cache.sh
  for e in EKYLIBRE_DATABASE_PASSWORD DEVISE_SECRET_KEY SECRET_KEY_BASE RAILS_ENV TENANT VERBOSE; do
  echo export $e=$(eval echo \$$e) >> env_cache.sh
  done
  docker cp env_cache.sh ekylibre_app_1:/app/config/env.sh
}

createdbuser() {
  docker exec ekylibre_db_1 psql -U postgres -c "CREATE USER ekylibre";
  docker exec ekylibre_db_1 psql -U postgres -c "ALTER USER ekylibre WITH ENCRYPTED PASSWORD '$EKYLIBRE_DATABASE_PASSWORD'"
}

setupappdbconfig() {
  docker cp ekylibre_app_1:/app/current/config/database.yml.sample database.yml
  sed -i 's/#host: 127.0.0.1/host: db/' database.yml
  sed -i "s/#password:/password: $EKYLIBRE_DATABASE_PASSWORD/" database.yml
  sed -i "s/#username: ekylibre/username: ekylibre/" database.yml
  docker cp database.yml ekylibre_app_1:/app/current/config/database.yml
}

rakedbadminaction() {
  docker exec ekylibre_db_1 psql -U postgres -c "ALTER USER ekylibre CREATEDB";
  docker exec ekylibre_db_1 psql -U postgres -c "ALTER USER ekylibre SUPERUSER"; # create extension

  docker exec ekylibre_app_1 env RAILS_ENV=$RAILS_ENV EKYLIBRE_DATABASE_PASSWORD=$EKYLIBRE_DATABASE_PASSWORD DEVISE_SECRET_KEY=$DEVISE_SECRET_KEY rake $*

  docker exec ekylibre_db_1 psql -U postgres -c "ALTER USER ekylibre NOCREATEDB";
  docker exec ekylibre_db_1 psql -U postgres -c "ALTER USER ekylibre NOSUPERUSER"; # create extension
}

assetprecompile() {
  echo "Precompiling assets (this takes a while...)"
  docker exec ekylibre_app_1 env RAILS_ENV=$RAILS_ENV EKYLIBRE_DATABASE_PASSWORD=$EKYLIBRE_DATABASE_PASSWORD DEVISE_SECRET_KEY=$DEVISE_SECRET_KEY rake assets:precompile
}


setupappenv

echo "creating config for app"
setupappdbconfig

echo "creating db user for app"
createdbuser
echo "creating app database"
rakedbadminaction db:create
echo "creating tenant $EKYLIBRE_TENANT"
docker exec ekylibre_app_1 sh -c ". /app/config/env.sh && rake tenant:create TENANT=$TENANT"
echo "migrating db"
docker exec ekylibre_app_1 sh -c ". /app/config/env.sh && rake db:migrate"

FIRST_RUN_TARGET=first_run
if [ "$TENANT" = demo ]; then
  echo "preparing demo"
  DEMO_BRANCH=f1694b2ad85ed7b67c3e1e83da0842a279c485ea
  [ -f first-run-demo-${DEMO_BRANCH}.tar.gz ] || curl -L -o first-run-demo-${DEMO_BRANCH}.tar.gz https://github.com/ekylibre/first_run-demo/tarball/$DEMO_BRANCH
  docker cp first-run-demo-${DEMO_BRANCH}.tar.gz ekylibre_app_1:/app/current/db/first_runs/ 
  docker exec ekylibre_app_1 sh -c "mkdir -p db/first_runs; cd db/first_runs && rm -rf ekylibre-first_run-demo-*; rm -f demo; tar xvf first-run-demo-${DEMO_BRANCH}.tar.gz && ln -s ekylibre-first_run-demo-* demo"
elif [ -d "$TENANT_FIRST_RUN_DIR" ]; then
  docker cp $TENANT_FIRST_RUN_DIR ekylibre_app_1:/app/current/db/first_runs/default
else
  FIRST_RUN_TARGET=first_run:default
fi
#sed -i "s/^host:.*/host: $EKYLIBRE_EXTERNAL_HOSTNAME/" db/first_runs/demo/manifest.yml
echo "applying setting on $TENANT"

docker exec ekylibre_app_1 env sh -c ". /app/config/env.sh && rake $FIRST_RUN_TARGET TENANT=$TENANT"

assetprecompile

docker exec -it ekylibre_app_1 touch /app/config/allow_start
# to revert allow_start:
# docker run --rm -v ekylibre_app-config:/app/config:z nginx rm /app/config/allow_start

