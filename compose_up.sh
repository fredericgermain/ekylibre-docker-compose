#!/bin/sh

. ./env.sh

# 1. Create docker network for frontend
docker network create $WEBPROXY_CONTAINER_PREFIX

# 3. Download the latest version of nginx.tmpl
[ -f nginx.tmpl ] || curl https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl > nginx.tmpl

docker-compose -p $WEBPROXY_CONTAINER_PREFIX -f docker-compose-webproxy-nginx.yml up -d

docker cp vhost.d_app_assets webproxy_nginx-gen_1:/etc/nginx/vhost.d/$EKYLIBRE_EXTERNAL_HOSTNAME
docker-compose -p $EKYLIBRE_CONTAINER_PREFIX -f docker-compose-alpine-http.yml up -d
# do ./bootstrap_config.sh to end configuration
