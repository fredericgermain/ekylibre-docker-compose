#!/bin/sh

echo "Waiting for bootstrap_config.sh"
while [ ! -f /app/config/env.sh ]; do
  sleep 1
done

echo "Waiting for /app/config/allow_start"
while [ ! -f /app/config/allow_start ]; do
  sleep 1
done

. /app/config/env.sh
exec $*
