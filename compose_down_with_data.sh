#!/bin/sh

. ./env.sh

docker-compose -p $EKYLIBRE_CONTAINER_PREFIX -f docker-compose-alpine-http.yml down
docker volume rm \
 ${EKYLIBRE_CONTAINER_PREFIX}_pgdata \
 ${EKYLIBRE_CONTAINER_PREFIX}_redis \
 ${EKYLIBRE_CONTAINER_PREFIX}_app-config \
 ${EKYLIBRE_CONTAINER_PREFIX}_app-logs \
 ${EKYLIBRE_CONTAINER_PREFIX}_app-assets

docker-compose -p $WEBPROXY_CONTAINER_PREFIX -f docker-compose-webproxy-nginx.yml down
docker volume rm \
 ${WEBPROXY_CONTAINER_PREFIX}-certs \
 ${WEBPROXY_CONTAINER_PREFIX}-conf.d \
 ${WEBPROXY_CONTAINER_PREFIX}-html \
 ${WEBPROXY_CONTAINER_PREFIX}-logs \
 ${WEBPROXY_CONTAINER_PREFIX}-vhost.d

docker network rm $WEBPROXY_CONTAINER_PREFIX 
