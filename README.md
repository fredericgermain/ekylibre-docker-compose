# Build docker image

The docker image is still not yet on hub.docker.com. You need to build it once:

```
cd app
./rebuild_docker_image.sh
```

# Running the server

## Running the webproxy + Ekylibre server
```
./compose_up.sh
```

And wait a few seconds that the servers are functionnal.

## Check the environment in the env.sh files

* EKYLIBRE_TENANT: if demo, demo data will automatically setup during bootstrap
* EKYLIBRE_EXTERNAL_HOSTNAME: the external hostname by which to access ekylibre. Use localhost for dev or demo, or see configuring HTTPS.
* WEBPROXY_PPORT: "9" will use tcp port 9080 and 9443 for the proxy, use "0" will use tcp port 80 and 443.

## Configuring the Ekylibre server
```
./bootstrap.sh
```

Note: generating app assets, or importing the demo data, is very long. Maybe there is a problem with the docker env on this step ?.

After it's done, you can go on http://localhost:9080, https://eky.myfarm.earth, depending on your configuration.

Unless you changed it in manifest.yml, admin login/password is admin@ekylibre.org/12345678.

# Removing everything (INCLUDING DATA)

```
./compose_down_with_data.sh
```

# Backups

Data are stored in the postgres database, and some docker volumes.

TODO

# Configuring HTTPS

TODO: just add https docker image, nothing much to be done.

* Points your DNS host name to your server, having an external IP address (OVH, ...), having both 80 and 443 ports free.
* Fill EKYLIBRE_EXTERNAL_HOSTNAME accordingly
* Bootstrap your server.
* You should have a server running with a Letencrypt certificate.

# About Alpine docker image

Alpine is a OS usually used to have smaller docker images. The comptatibility might still need to be worked compare to
the more used Ubuntu image.

There are a lot of 'sed' patches in the Dockerfile of the image. Ideally, we would need to patch the upstream source so
we have a clean image.

## proj4
It's a missing package from alpine <= 3.10.1, it's only in the edge image for far. So we're using the edge ruby, and the ruby 2.5 that comes along.

Maybe it'd be better to use slim OS for now for a small image, as in https://woile.github.io/posts/moving-from-docker-alpine-to-slim/ ?

## alpine and therubyracer
therubyracer gem packages uses precompiled binary by default, but these binary are not comptatible with ulibc used by Alpine distributions.

```
gem install therubyracer
irb(main):001:0> require 'v8'
LoadError: Error relocating /usr/local/bundle/gems/therubyracer-0.12.3/lib/v8/init.so: __vfprintf_chk: symbol not found - /usr/local/bundle/gems/therubyracer-0.12.3/lib/v8/init.so
```

There would be an --with-system-v8 options to build against a system libv8.so, but alpine do not provides a libv8 package. Also, mkmf.

```
export GEM_HOME=~/.gem_home
gem install libv8 -v "3.16.14.16" -- --with-system-v8
gem install therubyracer -v 0.12.3 -- --with-v8-include=/usr/include/node 
```

This fails to detect v8.h headeri from nodejs package in /usr/local/lib/ruby/2.2.0/mkmf.rb. It is because it's a C++ header and mkmf.rb is C only (could add "-x c++" flags in find_header/cpp?). Also, there is no package with libuv.so in Alpine, nodejs package only provides header. On ubuntu, there is a standalone libv8 package.

A alpine distrib seems to provide a valid gem-libv8 package, but I couldn't make it work with bundle: https://hub.docker.com/r/joenyland/ruby-alpine-libv8. Also, it feels messy to use a special base image for this problem.

There is a libv8-alpine gem, but I couldn't make it work along with therubyracer. 

For all these reason, alpine image use duktape instead of therubyracer as JS engine on Alpine

